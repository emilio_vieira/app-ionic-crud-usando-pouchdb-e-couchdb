
import { Injectable, NgZone } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { DataProvider } from '../data/data';

/*
  Generated class for the CommentsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CommentsProvider {

  comments: any = [];
  commentSubject: any = new Subject();

  constructor(public dataService: DataProvider, public zone: NgZone) {
  
    let $this = this;

    this.dataService.db.changes({live: true,since: 'now',include_docs: true}).on('change', (change)=>{

      if( (change.doc.type === 'comment' && $this.comments.length>0)){

        if(change.doc.post === $this.comments[0].post)
        {

          console.log($this.comments[0].post);
          
          $this.changeComment(change);    
          
        }
      }

      let objDoc = this.dataService.findDoc(change.id,this.comments);
      if(change.deleted && objDoc){
        this.comments.splice(objDoc.index,1);
        this.emitComments($this.comments[0].post)
      }

    });

  }


  getComments(postId){

    this.emitComments(postId);

    return this.commentSubject;

  }

  addComment(comment):void{
    this.dataService.db.post(comment);
  }

  

  emitComments(postId){

    let $this = this;
    
    this.zone.run( ()=>{

      this.dataService.db.query('comments/by_post_id',{key: postId})
        .then( (data)=>{
          
          let comments = data.rows.map( row => {
            return row.value;
          });

          $this.comments = comments;
          
          $this.commentSubject.next($this.comments);

        });
    })
  }

  changeComment(change): void{

    let changedIndex = null;
    let changedDoc = null;

    this.comments.forEach( (doc,index) => {
      if(doc._id === change.id){
        changedIndex = index;
        changedDoc = doc;
      }
    });

    if(change.deleted && changedDoc){
      this.comments.splice(changedIndex,1);
      console.log('comments deleted');
    } else {
    
      if(changedDoc){
        this.comments[changedIndex] = change.doc;
        console.log('comment updated');
      } else {
        this.comments.push(change.doc);
        console.log('comment new');
      }

    }

    this.commentSubject.next(this.comments);
    // this.emitComments(this.comments[0].post);
  }

}