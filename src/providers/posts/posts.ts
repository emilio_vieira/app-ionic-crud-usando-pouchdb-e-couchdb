import { Injectable, NgZone } from '@angular/core';
// import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { DataProvider } from '../data/data';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/from';

/*
  Generated class for the PostsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PostsProvider {

  posts: any;;

  // Assunto
  postSubject: any = new Subject();

  constructor(public dataService: DataProvider, public zone: NgZone) {
    // console.log('Hello PostsProvider Provider');
    this.dataService.db.changes({live:true,since:'now',include_docs:true})
      .on('change', (change)=>{

        if(change.doc.type === 'post'){
            // this.emitPosts();
            this.changePost(change);
        }

        let objDoc = this.dataService.findDoc(change.id,this.posts);
        if(change.deleted && objDoc){
          this.posts.splice(objDoc.index,1);
        }

        
      });
  }

  //Escutar modificacoes do CouchDB. Delete, Atualizacao ou Novo Documento.
  private changePost(change){

    let changedIndex = null;
    let changedDoc = null;

    // checar existencia no PouchDB(local). Permitir update/delete.
    this.posts.forEach( (doc,index) => {
      if(doc._id === change.id){
        changedIndex = index;
        changedDoc = doc;
      }
    });

    console.log('POSTS',change,changedDoc);
    //Checar se foi deletado no chance();
    if(change.deleted && changedDoc){
      this.posts.splice(changedIndex,1);
      console.log('POST deleted');
    } else {

      //Atualizar - Documento encontrado no array local do PouchDB
      if(changedDoc){
        this.posts[changedIndex] = change.doc;
        console.log('POST updated');
      }

      //Criar - Um novo documento
      else {
        this.posts.push(change.doc);
        console.log('new POST');
      }

    }

    
    this.emitPosts();
    // this.postSubject.next(this.posts);

  }
 
  private emitPosts():void {
    /* 
      Estudar conceito de ZONES. Para detecção de mudança de estados;
      https://www.joshmorony.com/understanding-zones-and-change-detection-in-ionic-2-angular-2/
    */
    this.zone.run( ()=>{

      this.dataService.db.query('posts/by_date_published').then( (data)=>{

        // pegar retornar de cada row, apenas object contido em value.
        let posts = data.rows.map(row=>{
          return row.value;
        });

        this.posts = posts;

        /**
         * Subscribes ou Se inscrever. Um Observable simplificado.
         * Pode ser atualizado com novos posts a qualquer momento.
         * E transmiti-lo para o Observable.
         * 
         * https://www.youtube.com/watch?v=RLZ-zJOzjyA
        */
        // informado retorno do assunto assinado. Subscribes.
        this.postSubject.next(this.posts); 

      });
    })
  }

  getPosts(){

    this.emitPosts();

    return this.postSubject;
  }

  addPost(post:any):void{
    this.dataService.db.put(post);
  }

  // getPostsOLD():Observable<any>{

  //   let endpoint_url_api = 'http://127.0.0.1:5984/couchblog/_design/posts/_view/by_date_published';

  //   return this.http.get(endpoint_url_api);
  //     // .map( (response:any) => {
  //     //   // return response.rows;
  //     //   return response;
  //     // });
   
  // }

}
