import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';


/*
  Generated class for the DataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DataProvider {

  db: any;
  remote: string = 'http://127.0.0.1:5984/couchblog';

  constructor(public http: HttpClient) {

    this.db = new PouchDB('couchdb');

    let options = {
      live: true,
      retry: true,
      continuos: true
    };

    this.db.sync(this.remote,options);

    console.log('Hello DataProvider Provider');
  }

  public findDoc(id,array):{index:number,doc:string}|null {
    let changedIndex = null;
    let changedDoc = null;
    
    array.forEach( (doc,index) => {
      if(doc._id === id){
        changedIndex = index;
        changedDoc = doc;
      }  
    });

    return !changedDoc ? null :{ index: changedIndex, doc: changedDoc };
  }


}
