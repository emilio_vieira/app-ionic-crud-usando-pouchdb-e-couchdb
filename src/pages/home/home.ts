import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { PostsProvider } from '../../providers/posts/posts';
import { AddPostPage } from '../add-post/add-post';
import { ViewPostPage } from '../view-post/view-post';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  posts:any[];

  constructor(public navCtrl: NavController, private postPrv: PostsProvider) {
  }

  ionViewDidLoad(){
    this.popularPosts();
  }

  // tratando com map do objeto do couchDB
  private popularPosts():void{
    this.postPrv.getPosts()
      .subscribe((posts) => {
        this.posts = posts;
      });
  }
  

  // Usando lambdas no subscribe
  private popularPosts2():void{
    this.postPrv.getPosts()
      .subscribe(
        // lambdas
        data => {
          this.posts = data;
        },
        error => {
          console.log(error);
        }
      );
  }

  pushAddPostPage(){
    this.navCtrl.push(AddPostPage);
  }

  viewPost(post){
    this.navCtrl.push(ViewPostPage,{post:post});
  }
}
