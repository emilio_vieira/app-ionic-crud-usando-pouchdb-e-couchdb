import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { PostsProvider } from '../../providers/posts/posts';

/**
 * Generated class for the AddPostPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-post',
  templateUrl: 'add-post.html',
})
export class AddPostPage {

  post:any =  {
    _id: null,
    author: 'Emílio Vieira',
    content: '',
    datePublished: '',
    dateUpdated:'',
    title: '',
    type:'post'
  }

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public postSrv: PostsProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddPostPage');
  }

  save(){
    // removendo não-alfanumériocos e substituido espaços por hiféns
    this.post._id = this.post.title.toLowerCase().replace(/ /g,'-').replace(/[^\w-]+/g,'')
    // Datas como string ISO 
    this.post.datePublished = new Date().toISOString();
    this.post.dateUpdated = new Date().toISOString();

    this.postSrv.addPost(this.post);

    this.navCtrl.pop();
  }

}
