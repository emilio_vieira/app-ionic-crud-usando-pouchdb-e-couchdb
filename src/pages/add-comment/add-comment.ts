import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CommentsProvider } from '../../providers/comments/comments';

/**
 * Generated class for the AddCommentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-comment',
  templateUrl: 'add-comment.html',
})
export class AddCommentPage {

  comment: any = {
    author: 'Josh Morony',
    content: '',
    datePublished: '',
    type: 'comment',
    post: null
};

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public commentService: CommentsProvider
  ) {
  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad AddCommentPage');
    this.comment.post = this.navParams.get('post')._id;
  }

  save(){
 
    // Generate computed fields
    this.comment.datePublished = new Date().toISOString();

    this.commentService.addComment(this.comment);

    this.navCtrl.pop();

  }

}
