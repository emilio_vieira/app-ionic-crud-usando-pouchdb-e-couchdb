import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CommentsProvider } from '../../providers/comments/comments';
import { AddCommentPage } from '../add-comment/add-comment';

/**
 * Generated class for the ViewPostPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-post',
  templateUrl: 'view-post.html',
  // changeDetection: ChangeDetectionStrategy.Default
})
export class ViewPostPage {

  post: any;
  comments: any[];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public commentService: CommentsProvider,
    private ref: ChangeDetectorRef
  ) {
   
  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad ViewPostPage');

    let postParam = this.navParams.get('post');

    if(postParam){
      
      this.post = postParam;

      this.commentService.getComments(this.post._id)
        .subscribe( (comments)=>{
          this.comments = comments;          
        });
    }
    
  }

  ngOnInit(){
    this.ref.detectChanges();
  }
  

  pushAddCommentPage(){
    this.navCtrl.push(AddCommentPage,{post:this.post})
  }

  ngOnDestroy() {
    // this.commentService.getComments(this.post._id).unsubscribe();
    // https://angular.io/api/core/ChangeDetectorRef#detectchanges
    this.ref.detach();
  }

}
